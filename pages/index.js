import getConfig from 'next/config'

const {
  publicRuntimeConfig: { HELLO },
} = getConfig()

const Home = () => {
  return (
    <div>
      <div>Test</div>
      <div>HELLO <b>{HELLO}</b></div>
      <div>process.env.HELLO <b>{process.env.HELLO}</b></div>
      <div>process.env.NEXT_PUBLIC_EXAMPLE_KEY <b>{process.env.NEXT_PUBLIC_EXAMPLE_KEY}</b></div>

    </div>
  )
}

export async function getServerSideProps(context) {
  return {
    props: {},
  }
}

export default Home;